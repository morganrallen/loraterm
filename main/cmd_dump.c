#include <stdio.h>
#include <string.h>
#include "esp_log.h"
#include "esp_console.h"
#include "argtable3/argtable3.h"

#include "esp32-lora.h"

static struct {
    struct arg_end *end;
    lora32_cfg_t  *lora;
} dump_args;

int dump(int argc, char **argv) {
  lora32_dump_regs((lora32_cfg_t*)dump_args.lora);

  return 0;
}

void cmd_dump_register(lora32_cfg_t *lora) {
  dump_args.end = arg_end(1);
  dump_args.lora = lora;

  const esp_console_cmd_t dump_cmd = {
    .command = "dump",
    .help = "Dump SX127x registers",
    .hint = NULL,
    .func = &dump,
    .argtable = &dump_args
  };

  ESP_ERROR_CHECK(esp_console_cmd_register(&dump_cmd));
}
