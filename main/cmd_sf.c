#include <stdio.h>
#include <string.h>
#include "esp_log.h"
#include "esp_console.h"
#include "argtable3/argtable3.h"

#include "esp32-lora.h"

static struct {
    struct arg_int *spreadfactor;
    struct arg_end *end;
    lora32_cfg_t  *lora;
} sf_args;

int set_sf(int argc, char **argv) {
  int nerrors = arg_parse(argc, argv, (void **) &sf_args);

  if (nerrors != 0) {
    arg_print_errors(stderr, sf_args.end, argv[0]);

    return 1;
  }

  lora32_set_spreadfactor((lora32_cfg_t*)sf_args.lora, sf_args.spreadfactor->ival[0]);

  return 0;
}

void cmd_sf_register(lora32_cfg_t *lora) {
  sf_args.spreadfactor = arg_int0(NULL, NULL, "<sf>", "Spreadfactor");
  sf_args.end = arg_end(1);
  sf_args.lora = lora;

  const esp_console_cmd_t sf_cmd = {
    .command = "spreadfactor",
    .help = "Set spreadfactor",
    .hint = NULL,
    .func = &set_sf,
    .argtable = &sf_args
  };

  ESP_ERROR_CHECK(esp_console_cmd_register(&sf_cmd));
}
