#include <stdio.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "driver/uart.h"
#include "esp_log.h"
#include "esp_debug_helpers.h"
#include "nvs_flash.h"
#include "esp_vfs_dev.h"
#include "esp_heap_trace.h"

#include "main.h"
#include "lorcomm.h"
#include "esp32-lora.h"
#include "console.h"
#include "ble.h"
#include "user_button.h"
#include "nmea_parser.h"

static const char *TAG = "loracom-main";

lora32_cfg_t lora;
uint8_t mac[6];
static gps_t gps;

static void handle_lora_txdone() {
  lora32_enable_continuous_rx(&lora);
}

void handle_lora_caddone(bool detected) {
  //ESP_LOGI(TAG, "CAD done: detected? %s", detected ? "true" : "false");

  if(detected) {
    lora32_enable_cad(&lora);
  } else {
    ESP_LOGI(TAG, "candone (no channel activity): enabling continuous RX");

    lora32_enable_continuous_rx(&lora);
  }
}

#ifdef CONFIG_LORCOMM_GPS_ENABLED
void gps_send_location() {
  vTaskDelay(3000 / portTICK_PERIOD_MS);

  ESP_LOGI(TAG, "Starting GPS loop");

  while(true) {
    if(gps.latitude != 0 && gps.longitude != 0) {
      /*
      lorcomm_send_location(&lora, (float)gps->latitude, (float)gps->longitude);

      ESP_LOGD(TAG, "%d/%d/%d %d:%d:%d => \r\n"
          "\t\t\t\t\t\tlatitude   = %.05f°N\r\n"
          "\t\t\t\t\t\tlongtitude = %.05f°E\r\n"
          "\t\t\t\t\t\taltitude   = %.02fm\r\n"
          "\t\t\t\t\t\tspeed      = %fm/s",
          gps->date.year, gps->date.month, gps->date.day,
          gps->tim.hour, gps->tim.minute, gps->tim.second,
          gps->latitude, gps->longitude, gps->altitude, gps->speed);
          */
    } else {
      ESP_LOGW(TAG, "GPS location not updated");
    }

    vTaskDelay((CONFIG_LORCOMM_GPS_PERIOD * 1000 * 60) / portTICK_PERIOD_MS);
  }
}

static void gps_event_handler(void *event_handler_arg, esp_event_base_t event_base, int32_t event_id, void *event_data) {
    static gps_t *gps = NULL;
    switch (event_id) {
    case GPS_UPDATE:
        gps = (gps_t *)event_data;
        //memcpy(&gps, (gps_t*)event_data, sizeof(gps_t));
        /* print information parsed from GPS statements */
        ESP_LOGD(TAG, "%d/%d/%d %d:%d:%d => \r\n"
                 "\t\t\t\t\t\tlatitude   = %.05f°N\r\n"
                 "\t\t\t\t\t\tlongtitude = %.05f°E\r\n"
                 "\t\t\t\t\t\taltitude   = %.02fm\r\n"
                 "\t\t\t\t\t\tspeed      = %fm/s",
                 gps->date.year + YEAR_BASE, gps->date.month, gps->date.day,
                 gps->tim.hour + TIME_ZONE, gps->tim.minute, gps->tim.second,
                 gps->latitude, gps->longitude, gps->altitude, gps->speed);
        break;
    case GPS_UNKNOWN:
        /* print unknown statements */
        ESP_LOGD(TAG, "Unknown statement:%s", (char *)event_data);
        break;
    default:
        break;
    }
}
#endif

void app_main(void) {
  esp_err_t err = nvs_flash_init();
  if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
      ESP_ERROR_CHECK(nvs_flash_erase());
      err = nvs_flash_init();
  }
  ESP_ERROR_CHECK( err );

  nvs_handle_t nvs_handle;
  err = nvs_open("config", NVS_READWRITE, &nvs_handle);
  if (err != ESP_OK) {
    printf("Error (%s) opening NVS handle!\n", esp_err_to_name(err));
  } else {
    lorcomm_cfg.nvs_handle = nvs_handle;
    ESP_LOGI(TAG, "nvs_handle: %d", nvs_handle);
  }

  esp_efuse_mac_get_default((uint8_t*)&mac);

  ESP_LOGI(TAG, "MAC: [%02X:%02X:%02X:%02X:%02X:%02X]", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);

  lora.bandwidth = B125;
  lora.codingRate = 5;
  lora.frequency = 915000000;
  lora.spreadingFactor = 11;
  lora.preamble = DEFAULT_PREAMBLE;
  lora.dio0 = CONFIG_LORA32_DIO0_PIN;
  lora.implicitHeader = false;
  lora.nss = CONFIG_LORA32_NSS_PIN;
  lora.reset = CONFIG_LORA32_RESET_PIN;
  lora.useCRC = false;
  lora.fifoIdx = 0;
  lora.receive = &lorcomm_handle_receive;
  lora.tx_done = &handle_lora_txdone;
  //lora.cad_done = &handle_lora_caddone;
  //lora.cad_detected = &handle_lora_caddetected;

  lora32_init(&lora);

  //double dr = lora32_calc_datarate(&lora);
  //ESP_LOGI(TAG, "data rate: %fbps", dr);

  lora32_dump_regs(&lora);

  lorcomm_cfg.lora = &lora;

  //ble_init();

  lora32_enable_continuous_rx(&lora);

#ifdef CONFIG_LORCOMM_GPS_ENABLED
  ESP_LOGI(TAG, "sizeof(gps_t): %d", sizeof(gps_t));

  nmea_parser_config_t config = NMEA_PARSER_CONFIG_DEFAULT();
  config.uart.rx_pin = CONFIG_LORCOMM_GPS_RX;

  nmea_parser_handle_t nmea_hdl = nmea_parser_init(&config);
  nmea_parser_add_handler(nmea_hdl, gps_event_handler, NULL);
#endif


#ifdef CONFIG_LORCOMM_BUTTON_ACTION_ENABLED
  user_button_init();
#endif

#ifdef CONFIG_LORCOMM_GPS_PERIOD > 0
  xTaskCreate(gps_send_location, "periodic", 1024, NULL, tskIDLE_PRIORITY + 3, NULL);
#endif

  xTaskCreate(console_task, "console", 4048, &lorcomm_cfg, tskIDLE_PRIORITY + 3, NULL);

  while(1) {
    vTaskDelay(1000 / portTICK_PERIOD_MS);
  }
}
