#include <stdio.h>
#include <string.h>

#include "esp_system.h"
#include "esp_log.h"
#include "esp_console.h"
#include "esp_vfs_dev.h"
#include "driver/uart.h"
#include "linenoise/linenoise.h"
#include "argtable3/argtable3.h"
#include "esp32-lora.h"

#include "cmd_lora.h"
#include "main.h"

// main console setup and task, assume mostly copied from example
void console_task(void *args) {
  // disable buffering
  setvbuf(stdin, NULL, _IONBF, 0);

  // setup line endings
  esp_vfs_dev_uart_set_rx_line_endings(ESP_LINE_ENDINGS_CR);
  esp_vfs_dev_uart_set_tx_line_endings(ESP_LINE_ENDINGS_CRLF);

  const uart_config_t uart_config = {
    .baud_rate = CONFIG_ESP_CONSOLE_UART_BAUDRATE,
    .data_bits = UART_DATA_8_BITS,
    .parity = UART_PARITY_DISABLE,
    .stop_bits = UART_STOP_BITS_1,
    .use_ref_tick = true
  };

  ESP_ERROR_CHECK(uart_param_config(CONFIG_ESP_CONSOLE_UART_NUM, &uart_config));

  ESP_ERROR_CHECK(uart_driver_install(CONFIG_ESP_CONSOLE_UART_NUM, 256, 0, 0, NULL, 0));

  esp_vfs_dev_uart_use_driver(CONFIG_ESP_CONSOLE_UART_NUM);

  esp_console_config_t console_config = {
    .max_cmdline_args = 8,
    .max_cmdline_length = 256,
    .hint_color = atoi(LOG_COLOR_CYAN)
  };

  ESP_ERROR_CHECK(esp_console_init(&console_config));

  linenoiseSetMultiLine(1);
  linenoiseSetCompletionCallback(&esp_console_get_completion);
  linenoiseSetHintsCallback((linenoiseHintsCallback*) &esp_console_get_hint);
  linenoiseHistorySetMaxLen(100);

  esp_console_register_help_command();
  cmd_bw_register(lorcomm_cfg.lora);
  cmd_cr_register(lorcomm_cfg.lora);
  cmd_sf_register(lorcomm_cfg.lora);
  cmd_preamble_register(lorcomm_cfg.lora);
  cmd_send_register(lorcomm_cfg.lora);
  cmd_dump_register(lorcomm_cfg.lora);
  cmd_hostname_register(lorcomm_cfg.lora);
  cmd_ota_register(lorcomm_cfg.lora);

  char *prompt;

  while(true) {
    asprintf(&prompt, LOG_COLOR_I "%s bw: %d sf: %d cr: %d> " LOG_RESET_COLOR,
      lorcomm_cfg.hostname,
      lora.bandwidth,
      lora.spreadingFactor,
      lora.codingRate
    );

    linenoiseSetDumbMode(1);
    char* line = linenoise(prompt);

    free(prompt);

    if (line == NULL) { /* Ignore empty lines */
      continue;
    }

    linenoiseHistoryAdd(line);

    int ret;
    esp_err_t err = esp_console_run(line, &ret);

    if (err == ESP_ERR_NOT_FOUND) {
      printf("Unrecognized command\n");
    } else if (err == ESP_ERR_INVALID_ARG) {
    } else if (err == ESP_OK && ret != ESP_OK) {
      printf("Command returned non-zero error code: 0x%x (%s)\n", ret, esp_err_to_name(err));
    } else if (err != ESP_OK) {
      printf("Internal error: %s\n", esp_err_to_name(err));
    }

    linenoiseFree(line);
  }
}
