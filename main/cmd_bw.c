#include <stdio.h>
#include <string.h>
#include "esp_log.h"
#include "esp_console.h"
#include "argtable3/argtable3.h"

#include "cmd_lora.h"
#include "esp32-lora.h"

static struct {
    struct arg_int *bandwidth;
    struct arg_end *end;
    lora32_cfg_t  *lora;
} bw_args;

int set_bw(int argc, char **argv) {
  if(argc == 1) {
    printf("     > %d\n", bw_args.lora->bandwidth);

    return 0;
  }

  int nerrors = arg_parse(argc, argv, (void **) &bw_args);

  if (nerrors != 0) {
    arg_print_errors(stderr, bw_args.end, argv[0]);

    return 1;
  }

  lora32_set_bandwidth((lora32_cfg_t*)bw_args.lora, bw_args.bandwidth->ival[0]);

  return 0;
}

void cmd_bw_register(lora32_cfg_t *lora) {
  bw_args.bandwidth = arg_int0(NULL, NULL, "<bw>", "Bandwidth");
  bw_args.end = arg_end(1);
  bw_args.lora = lora;

  const esp_console_cmd_t bw_cmd = {
    .command = "bandwidth",
    .help = "Set bandwidth",
    .hint = NULL,
    .func = &set_bw,
    .argtable = &bw_args
  };

  ESP_ERROR_CHECK(esp_console_cmd_register(&bw_cmd));
}
