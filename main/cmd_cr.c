#include <stdio.h>
#include <string.h>
#include "esp_log.h"
#include "esp_console.h"
#include "argtable3/argtable3.h"

#include "cmd_lora.h"
#include "esp32-lora.h"

static struct {
    struct arg_int *codingrate;
    struct arg_end *end;
    lora32_cfg_t  *lora;
} cr_args;

int set_cr(int argc, char **argv) {
  int nerrors = arg_parse(argc, argv, (void **) &cr_args);

  if (nerrors != 0) {
    arg_print_errors(stderr, cr_args.end, argv[0]);

    return 1;
  }

  lora32_set_coding_rate((lora32_cfg_t*)cr_args.lora, cr_args.codingrate->ival[0]);

  return 0;
}

void cmd_cr_register(lora32_cfg_t *lora) {
  cr_args.codingrate = arg_int0(NULL, NULL, "<cr>", "Coding Rate");
  cr_args.end = arg_end(1);
  cr_args.lora = lora;

  const esp_console_cmd_t cr_cmd = {
    .command = "codingrate",
    .help = "Set codingrate",
    .hint = NULL,
    .func = &set_cr,
    .argtable = &cr_args
  };

  ESP_ERROR_CHECK(esp_console_cmd_register(&cr_cmd));
}
