#include <stdio.h>
#include <string.h>
#include "esp_log.h"
#include "esp_console.h"
#include "argtable3/argtable3.h"
#include "nvs_flash.h"
#include "esp_vfs_dev.h"

#include "esp32-lora.h"
#include "lorcomm.h"
#include "main.h"

static struct {
    struct arg_str *hostname;
    struct arg_end *end;
    lora32_cfg_t  *lora;
} hostname_args;

int hostname(int argc, char **argv) {
  int nerrors = arg_parse(argc, argv, (void **)&hostname_args);

  if (nerrors != 0) {
    arg_print_errors(stderr, hostname_args.end, argv[0]);

    return 1;
  }

  if(argc == 1) {
    printf("  > %s\n", lorcomm_cfg.hostname);
  } else {
    asprintf(&lorcomm_cfg.hostname, "%s", hostname_args.hostname->sval[0]);

    nvs_set_str(lorcomm_cfg.nvs_handle, "hostname", hostname_args.hostname->sval[0]);
    esp_err_t err = nvs_commit(lorcomm_cfg.nvs_handle);
    printf((err != ESP_OK) ? "Failed!\n" : "Done\n");
  }

  return 0;
}

void cmd_hostname_register(lora32_cfg_t *lora) {
  hostname_args.hostname = arg_strn(NULL, NULL, "\"hostname\"", 0, 1, "hostname");
  hostname_args.end = arg_end(1);
  hostname_args.lora = lora;

  size_t hostname_len;

  printf("nvs_handle: %d\n\n\n", lorcomm_cfg.nvs_handle);
  esp_err_t err = nvs_get_str(lorcomm_cfg.nvs_handle, "hostname", NULL, &hostname_len);

  if(err == ESP_OK && hostname_len > 0) {
    printf("hostname_len: %d\n", hostname_len);
    lorcomm_cfg.hostname = malloc(hostname_len);

    nvs_get_str(lorcomm_cfg.nvs_handle, "hostname", lorcomm_cfg.hostname, &hostname_len);
    printf("hostname: %s\n", lorcomm_cfg.hostname);
  } else {
    lorcomm_cfg.hostname = malloc(7);
    memcpy(lorcomm_cfg.hostname, "lorcomm\0", 8);
  }

  const esp_console_cmd_t hostname_cmd = {
    .command = "hostname",
    .help = "Set/Get Hostname",
    .hint = NULL,
    .func = &hostname,
    .argtable = &hostname_args
  };

  ESP_ERROR_CHECK(esp_console_cmd_register(&hostname_cmd));
}
