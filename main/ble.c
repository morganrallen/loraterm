#ifdef CONFIG_LORCOMM_BLE_ENABLED
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"

#include "esp_nimble_hci.h"
#include "nimble/nimble_port.h"
#include "nimble/nimble_port_freertos.h"
#include "host/ble_hs.h"
#include "host/util/util.h"
#include "services/gap/ble_svc_gap.h"
#include "services/bas/ble_svc_bas.h"

#include "main.h"
#include "ble.h"
#include "esp32-lora.h"

#define TAG                       "BLE"
#define CONFIG_CACO_MAX_SERVICES (10)
#define CACO_BLE_ADV_PERIOD      (4000)

static const char *device_name = "LoRaComm";

static uint8_t blehr_addr_type;
static uint8_t callback_handler_count = 0;
static uint16_t conn_handle;
static event_callback_handle_t callback_handlers[CONFIG_CACO_MAX_SERVICES] = { 0 };

static int ble_gap_event(struct ble_gap_event *event, void *arg);
static uint16_t ble_svc_handle;
static int ble_svc_access(uint16_t conn_handle, uint16_t attr_handle, struct ble_gatt_access_ctxt *ctxt, void *arg);

#define BLE_SERVICE_LORA        (0x4200)
#define BLE_SERVICE_LORA_BW     (0x4201)
#define BLE_SERVICE_LORA_CR     (0x4202)
#define BLE_SERVICE_LORA_SF     (0x4203)
#define BLE_SERVICE_LORA_PRE    (0x4204)
#define BLE_SERVICE_LORA_TX     (0x4205)
#define BLE_SERVICE_LORA_RX     (0x4206)
#define BLE_SERVICE_LORA_FREQ   (0x4207)

static const struct ble_gatt_svc_def service_defs[] = {{
  .type = BLE_GATT_SVC_TYPE_PRIMARY,
  .uuid = BLE_UUID16_DECLARE(BLE_SERVICE_LORA),
  .characteristics = (struct ble_gatt_chr_def[]) { {
    .uuid = BLE_UUID16_DECLARE(BLE_SERVICE_LORA_BW),
    .access_cb = ble_svc_access,
    .val_handle = &ble_svc_handle,
    .flags = BLE_GATT_CHR_F_WRITE | BLE_GATT_CHR_F_READ
  }, {
    .uuid = BLE_UUID16_DECLARE(BLE_SERVICE_LORA_CR),
    .access_cb = ble_svc_access,
    .val_handle = &ble_svc_handle,
    .flags = BLE_GATT_CHR_F_WRITE | BLE_GATT_CHR_F_READ
  }, {
    .uuid = BLE_UUID16_DECLARE(BLE_SERVICE_LORA_SF),
    .access_cb = ble_svc_access,
    .val_handle = &ble_svc_handle,
    .flags = BLE_GATT_CHR_F_WRITE | BLE_GATT_CHR_F_READ
  }, {
    .uuid = BLE_UUID16_DECLARE(BLE_SERVICE_LORA_PRE),
    .access_cb = ble_svc_access,
    .val_handle = &ble_svc_handle,
    .flags = BLE_GATT_CHR_F_WRITE | BLE_GATT_CHR_F_READ
  }, {
    .uuid = BLE_UUID16_DECLARE(BLE_SERVICE_LORA_TX),
    .access_cb = ble_svc_access,
    .val_handle = &ble_svc_handle,
    .flags = BLE_GATT_CHR_F_WRITE
  }, {
    .uuid = BLE_UUID16_DECLARE(BLE_SERVICE_LORA_RX),
    .access_cb = ble_svc_access,
    .val_handle = &ble_svc_handle,
    .flags = BLE_GATT_CHR_F_NOTIFY | BLE_GATT_CHR_F_READ
  }, {
    .uuid = BLE_UUID16_DECLARE(BLE_SERVICE_LORA_FREQ),
    .access_cb = ble_svc_access,
    .val_handle = &ble_svc_handle,
    .flags = BLE_GATT_CHR_F_WRITE | BLE_GATT_CHR_F_READ
  }, {
    0, /* No more characteristics in this service. */
  } },
},

{ 0 }
};


static int ble_svc_access(uint16_t conn_handle, uint16_t attr_handle, struct ble_gatt_access_ctxt *ctxt, void *arg) {
  uint16_t uuid16 = ble_uuid_u16(ctxt->chr->uuid);

  esp_err_t err = ble_gatts_count_cfg(service_defs);

  ESP_LOGI(TAG, "0x%02X access: %d", uuid16, ctxt->op);

  switch(uuid16) {
    case BLE_SERVICE_LORA_BW:
      os_mbuf_append(ctxt->om, (void*)&lora.bandwidth, 1);
      break;

    case BLE_SERVICE_LORA_CR:
      os_mbuf_append(ctxt->om, (void*)&lora.codingRate, 1);
      break;

    case BLE_SERVICE_LORA_SF:
      os_mbuf_append(ctxt->om, (void*)&lora.spreadingFactor, 1);
      break;

    case BLE_SERVICE_LORA_FREQ:
      os_mbuf_append(ctxt->om, (void*)&lora.frequency, 1);
      break;

    case 0x2901:
        os_mbuf_append(ctxt->om, "hello", 5);
      break;
  }

  return 0;
}

static void ble_advertise(void) {
  struct ble_gap_adv_params adv_params;
  struct ble_hs_adv_fields fields;
  int rc;

  /*
   *  Set the advertisement data included in our advertisements:
   *     o Flags (indicates advertisement type and other general info)
   *     o Advertising tx power
   *     o Device name
   */
  memset(&fields, 0, sizeof(fields));

  /*
   * Advertise two flags:
   *      o Discoverability in forthcoming advertisement (general)
   *      o BLE-only (BR/EDR unsupported)
   */
  fields.flags = BLE_HS_ADV_F_DISC_GEN |
    BLE_HS_ADV_F_BREDR_UNSUP;

  /*
   * Indicate that the TX power level field should be included; have the
   * stack fill this value automatically.  This is done by assigning the
   * special value BLE_HS_ADV_TX_PWR_LVL_AUTO.
   */
  fields.tx_pwr_lvl_is_present = 1;
  fields.tx_pwr_lvl = BLE_HS_ADV_TX_PWR_LVL_AUTO;

  fields.name = (uint8_t *)device_name;
  fields.name_len = strlen(device_name);
  fields.name_is_complete = 1;

  rc = ble_gap_adv_set_fields(&fields);
  if (rc != 0) {
    MODLOG_DFLT(ERROR, "error setting advertisement data; rc=%d\n", rc);
    return;
  }

  /* Begin advertising */
  memset(&adv_params, 0, sizeof(adv_params));
  adv_params.conn_mode = BLE_GAP_CONN_MODE_UND;
  adv_params.disc_mode = BLE_GAP_DISC_MODE_GEN;
  rc = ble_gap_adv_start(blehr_addr_type, NULL, BLE_HS_FOREVER,
      &adv_params, ble_gap_event, NULL);
  if (rc != 0) {
    MODLOG_DFLT(ERROR, "error enabling advertisement; rc=%d\n", rc);
    return;
  }
}

static int ble_gap_event(struct ble_gap_event *event, void *arg) {
  uint8_t i = 0;

  switch (event->type) {
    case BLE_GAP_EVENT_CONNECT:
      /* A new connection was established or a connection attempt failed */
      MODLOG_DFLT(INFO, "connection %s; status=%d\n",
          event->connect.status == 0 ? "established" : "failed",
          event->connect.status);

      if (event->connect.status != 0) {
        /* Connection failed; resume advertising */
        ble_advertise();
      }
      conn_handle = event->connect.conn_handle;
      break;

    case BLE_GAP_EVENT_DISCONNECT:
      ESP_LOGI(TAG, "disconnect; reason=%d\n", event->disconnect.reason);

      /* Connection terminated; resume advertising */
      ble_advertise();
      break;

    case BLE_GAP_EVENT_ADV_COMPLETE:
      ESP_LOGI(TAG, "adv complete\n");

      ble_advertise();
      break;

    case BLE_GAP_EVENT_SUBSCRIBE:
      ESP_LOGI(TAG, "handling BLE_GAP_EVENT_SUBSCRIBE");
      ESP_LOGI(TAG, "attr_handle: %d", event->subscribe.attr_handle);
      ESP_LOGI(TAG, "callback_handler_count: %d", callback_handler_count);

      for(i = 0; (i < CONFIG_CACO_MAX_SERVICES); i++) {
        if(callback_handlers[i].handle != NULL) continue;

        ESP_LOGI(TAG, "callback_handler: %d", (uint32_t)(callback_handlers[i].handle));

        if (event->subscribe.attr_handle == (uint32_t)(*callback_handlers[i].handle)) {
          callback_handlers[i].callback(event, arg);
        }
      }
      break;

    case BLE_GAP_EVENT_CONN_UPDATE:
      ESP_LOGI(TAG, "BLE_GAP_EVENT_CONN_UPDATE, status: %d", event->enc_change.status);
      break;


    case BLE_GAP_EVENT_ENC_CHANGE:
      ESP_LOGI(TAG, "BLE_GAP_EVENT_ENC_CHANGE");
      break;

    case BLE_GAP_EVENT_MTU:
      ESP_LOGI(TAG, "mtu update event; conn_handle=%d mtu=%d\n",
          event->mtu.conn_handle,
          event->mtu.value);
      break;
  }

  return 0;
}

static void on_sync() {
  ESP_LOGI(TAG, "on_sync");

  int err;

  err = ble_hs_id_infer_auto(0, &blehr_addr_type);
  ESP_ERROR_CHECK(err);

  uint8_t addr_val[6] = {0};
  err = ble_hs_id_copy_addr(blehr_addr_type, addr_val, NULL);
  ESP_ERROR_CHECK(err);

  ble_advertise();
}

static void on_reset(int reason) {
  ESP_LOGI(TAG, "on_reset, reason: %d", reason);
}

void nimble_host_task(void *param) {
  nimble_port_run();
}

void ble_init() {
  esp_err_t err;

  memset(&callback_handlers, 0, sizeof(event_callback_handle_t) * CONFIG_CACO_MAX_SERVICES);

  ESP_ERROR_CHECK(esp_nimble_hci_and_controller_init());

  nimble_port_init();

  ble_hs_cfg.sync_cb = on_sync;
  ble_hs_cfg.reset_cb = on_reset;

  // initialize services
  err = ble_gatts_count_cfg(service_defs);
  ESP_ERROR_CHECK(err);

  err = ble_gatts_add_svcs(service_defs);
  ESP_ERROR_CHECK(err);

  err = ble_svc_gap_device_name_set(device_name);
  ESP_ERROR_CHECK(err);

  nimble_port_freertos_init(nimble_host_task);
}
#endif // CONFIG_LORCOMM_BLE_ENABLED
