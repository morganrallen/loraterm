#include <stdio.h>
#include <string.h>
#include "esp_log.h"
#include "esp_console.h"
#include "esp_partition.h"
#include "esp_ota_ops.h"
#include "argtable3/argtable3.h"

#include "esp32-lora.h"

static struct {
    struct arg_end *end;
    lora32_cfg_t  *lora;
} ota_args;

int ota(int argc, char **argv) {
  esp_partition_t *factory = esp_partition_find_first(ESP_PARTITION_TYPE_APP, ESP_PARTITION_SUBTYPE_APP_FACTORY, "factory");

  if(factory != NULL) {
    ESP_LOGI("cmd_ota", "found partition: addr: 0x%02X size: %d", factory->address, factory->size);

    esp_ota_set_boot_partition(factory);

    esp_restart();
  } else {
    ESP_LOGE("cmd_ota", "failed to find factory partition");
  }

  return 0;
}

void cmd_ota_register(lora32_cfg_t *lora) {
    
  ota_args.end = arg_end(1);
  ota_args.lora = lora;

  const esp_console_cmd_t ota_cmd = {
    .command = "ota",
    .help = "Reboots into WiFi OTA mode",
    .hint = NULL,
    .func = &ota,
    .argtable = &ota_args
  };

  ESP_ERROR_CHECK(esp_console_cmd_register(&ota_cmd));
}
