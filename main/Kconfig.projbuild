menu "LoRComm Config"
  menuconfig LORCOMM_GPS_ENABLED
    bool "Enable GPS receiver"
    help
      Enables attached GPS receiver

    if LORCOMM_GPS_ENABLED
      config LORCOMM_GPS_RX
        int "GPS RX Pin"
        default 13
        help
          GPIO to receive data from GPS receiver

      config LORCOMM_GPS_BAUD
        int "GPS RX Baud Rate"
        default 9600
        help
          Baud rate to listen on GPS receiver

      config LORCOMM_GPS_TZ
        int "GPS RX Pin"
        default -8
        help
          Default Timezone

      config LORCOMM_GPS_PERIOD
        int "Send location every N minute"
        default 0
        help
          Periodically send GPS location.

      config NMEA_PARSER_RING_BUFFER_SIZE
          int "NMEA Parser Ring Buffer Size"
          range 0 2048
          default 1024
          help
              Size of the ring buffer used for UART Rx channel.

      config NMEA_PARSER_TASK_STACK_SIZE
          int "NMEA Parser Task Stack Size"
          range 0 4096
          default 2048
          help
              Stack size of NMEA Parser task.

      config NMEA_PARSER_TASK_PRIORITY
          int "NMEA Parser Task Priority"
          range 0 24
          default 2
          help
              Priority of NMEA Parser task.

      menu "NMEA Statement Support"
          comment "At least one statement must be selected"
          config NMEA_STATEMENT_GGA
              bool "GGA Statement"
              default y
              help
                  Enabling this option will parse the following parameter from GGA statement:

                  - Latitude, Longitude, Altitude;
                  - Number of satellites in use, fix status (no fix, GPS, DGPS), UTC time;

          config NMEA_STATEMENT_GSA
              bool "GSA Statement"
              default y
              help
                  Enabling this option will parse the following parameter from GSA statement:

                  - Position/Vertical/Horizontal dilution of precision;
                  - Fix mode (no fix, 2D, 3D fix);
                  - IDs of satellites in use;

          config NMEA_STATEMENT_GSV
              bool "GSV Statement"
              default y
              help
                  Enabling this option will parse the following parameter from GSV statement:

                  - Number of satellites in view;
                  - Optional details of each satellite in view;

          config NMEA_STATEMENT_RMC
              bool "RMC Statement"
              default y
              help
                  Enabling this option will parse the following parameter from RMC statement:

                  - Validity of GPS signal;
                  - Ground speed (knots) and course over ground (degrees);
                  - Magnetic variation;
                  - UTC date;

          config NMEA_STATEMENT_GLL
              bool "GLL Statement"
              default y
              help
                  Enabling this option will parse the following parameter from GLL statement:

                  - Latitude, Longitude;
                  - UTC time;

          config NMEA_STATEMENT_VTG
              bool "VTG Statement"
              default y
              help
                  Enabling this option will parse the following parameter from VTG statement:

                  - Ground speed (knots, km/h) and course over ground (degrees);
                  - Magnetic variation;

      endmenu
    endif

  choice LORCOMM_BUTTON_ACTION
    bool "User Button Action"
    default LORCOMM_BUTTON_ACTION_HELO
        help
          Selection action to be performed when the user button
          is pressed.

        config LORCOMM_BUTTON_ACTION_NONE
            bool "Do nothing"
            help
              Do nothing

        config LORCOMM_BUTTON_ACTION_HELO
            bool "Send HELO"
            help
              Send out a HELO packet and listen for response

        if LORCOMM_GPS_ENABLED
          config LORCOMM_BUTTON_ACTION_GPS
              bool "Send GPS"
              help
                  Sends GPS coordinates and listens for ACK.
        endif
    endchoice

    config LORCOMM_BUTTON_ACTION_ENABLED
      bool
        default n if LORCOMM_BUTTON_ACTION_NONE
        default y

    if LORCOMM_BUTTON_ACTION_ENABLED
      config LORCOMM_BUTTON_GPIO
        int "User Button GPIO"
        default 0
        help
          GPIO Pin for User Button
    endif
endmenu
