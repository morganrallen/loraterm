#include <stdio.h>
#include <string.h>
#include "esp_log.h"
#include "esp_console.h"
#include "argtable3/argtable3.h"

#include "cmd_lora.h"
#include "esp32-lora.h"

static struct {
    struct arg_int *codingrate;
    struct arg_end *end;
    lora32_cfg_t  *lora;
} preamble_args;

int set_preamble(int argc, char **argv) {
  int nerrors = arg_parse(argc, argv, (void **) &preamble_args);

  if (nerrors != 0) {
    arg_print_errors(stderr, preamble_args.end, argv[0]);

    return 1;
  }

  //lora32_set_preamble((lora32_cfg_t*)preamble_args.lora, preamble_args.codingrate->ival[0]);


  return 0;
}

void cmd_preamble_register(lora32_cfg_t *lora) {
  preamble_args.codingrate = arg_int0(NULL, NULL, "<preamble>", "Preamble");
  preamble_args.end = arg_end(1);
  preamble_args.lora = lora;

  const esp_console_cmd_t preamble_cmd = {
    .command = "preamble",
    .help = "Set preamble",
    .hint = NULL,
    .func = &set_preamble,
    .argtable = &preamble_args
  };

  ESP_ERROR_CHECK(esp_console_cmd_register(&preamble_cmd));
}
