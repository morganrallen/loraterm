#include <stdio.h>
#include <string.h>
#include "esp_log.h"
#include "esp_console.h"
#include "argtable3/argtable3.h"

#include "esp32-lora.h"
#include "lorcomm.h"
#include "main.h"

static struct {
    struct arg_str *msg;
    struct arg_end *end;
    lora32_cfg_t  *lora;
} send_args;

int send(int argc, char **argv) {
  int nerrors = arg_parse(argc, argv, (void **) &send_args);

  if (nerrors != 0) {
    arg_print_errors(stderr, send_args.end, argv[0]);

    return 1;
  }

  lorcomm_send_message((lora32_cfg_t*)send_args.lora, send_args.msg->sval[0]);

  return 0;
}

void cmd_send_register(lora32_cfg_t *lora) {
  send_args.msg = arg_strn(NULL, NULL, "\"message\"", 1, 10, "message");
  send_args.end = arg_end(1);
  send_args.lora = lora;

  const esp_console_cmd_t send_cmd = {
    .command = "send",
    .help = "Send message",
    .hint = NULL,
    .func = &send,
    .argtable = &send_args
  };

  ESP_ERROR_CHECK(esp_console_cmd_register(&send_cmd));
}
