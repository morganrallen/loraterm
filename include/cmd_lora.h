#ifndef __CMD_LORA_H_
#define __CMD_LORA_H_

#ifdef __cplusplus
extern "C" {
#endif

void cmd_bw_register();
void cmd_cr_register();
void cmd_sf_register();
void cmd_preamble_register();
void cmd_send_register();
void cmd_dump_register();
void cmd_hostname_register();
void cmd_ota_register();

#ifdef __cplusplus
}
#endif

#endif
