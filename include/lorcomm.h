#ifndef __LORCOMM_H_H
#define __LORCOMM_H_H

// disable memory alignment
#pragma pack(push, 1)
typedef struct {
  uint8_t id;
  uint8_t src[3];
  uint8_t dst[3];
  uint8_t type;
  void *payload;
} lora_packet;

// this is wrong, why is working correctly? should be 7
#define LORA_PACKET_HEADER_SIZE (8)

typedef struct {
  uint8_t length;
  char *msg;
} lora_msg;

#define LORA_MSG_HEADER_SIZE (1)

typedef struct {
  uint8_t origin[3];
  uint8_t type; // same as packet type
  void *payload;
} lora_routing;

#define LORA_ROUTING_HEADER_SIZE (4)

#pragma pack(pop)

enum lora_msg_type {
  TEXT = 0,
  ROUTING,
  GPS
};

extern uint8_t lormac[3];

void lorcomm_handle_receive(uint8_t size);
void lorcomm_send_message(lora32_cfg_t *lora, char *text);
void lorcomm_send_location(lora32_cfg_t *lora, float lat, float lon);

#endif
