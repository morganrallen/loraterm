#ifndef __MAIN_H_
#define __MAIN_H_

#include "nvs_flash.h"
#include "nmea_parser.h"

#include "esp32-lora.h"

#define TIME_ZONE CONFIG_LORCOMM_GPS_TZ
#define YEAR_BASE (2000)

//gps_t *gps;
extern uint8_t mac[6];
extern lora32_cfg_t lora;

struct lorcomm_cfg {
  char *hostname;

  lora32_cfg_t *lora;
  nvs_handle_t nvs_handle;
} lorcomm_cfg;


#endif
